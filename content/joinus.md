+++
title = "Join the Veloren community!"
description = "Join the Veloren community!"

weight = 0
+++

Veloren is an open-source, community-driven project! Join us in making the game the best it can be.

* [Gitlab — Browse the source code](https://gitlab.com/veloren/veloren)
* [Discord — Join the discussion](https://discord.gg/ecUxc9N)
* [Matrix — Join the discussion](https://matrix.to/#/#veloren-space:fachschaften.org) | [(Room list)](@/matrix_room_list.md) | WIP
* [Reddit — Get the latest updates to this blog at r/Veloren](https://www.reddit.com/r/Veloren/)
* [Youtube - Official Videos](https://youtube.com/channel/UCmRjlnKnSRRihWPPNasl_Qw) | [(RSS)](https://www.youtube.com/feeds/videos.xml?channel_id=UCmRjlnKnSRRihWPPNasl_Qw) | [(Invidious)](https://yewtu.be/channel/UCmRjlnKnSRRihWPPNasl_Qw) | [(Invidious - RSS)](https://yewtu.be/feed/channel/UCmRjlnKnSRRihWPPNasl_Qw)
* [Twitch - Developer Channel](https://www.twitch.tv/veloren_dev/videos) | [(Live Channels)](https://www.twitch.tv/directory/game/Veloren)
* [Veloren Wiki — Contribute or read articles about every aspect of the game](https://wiki.veloren.net/)

*It has unfortunately come to our attention that Veloren's assets, code, and imagery have, on occasion, been used to
promote malicious scams such as 'pump-and-dump' schemes, often involving cryptocurrency or NFTs. The core development
team is clear: Veloren is not, and will never be, a for-profit project, nor do we wish to be associated with regressive,
socially harmful technology like cryptocurrency.*
